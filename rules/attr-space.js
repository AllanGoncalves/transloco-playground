module.exports = function(HTMLHint) {
  HTMLHint.addRule({
    id: 'attr-space',
    description:
      'Attributes cannot have useless whitespace between "=" and attribute name or attribute value.',
    init: function(parser, reporter) {
      var self = this;

      function handleTagStart(event) {
        var col = event.col + event.tagName.length + 1;

        event.attrs
          .filter(function(attr) {
            return attr.value;
          })
          .forEach(function(attr) {
            var rawAttr = attr.raw;
            var indexOfEqualSign = rawAttr.indexOf('=');

            if (rawAttr.charAt(indexOfEqualSign - 1) === ' ') {
              reporter.warn(
                'Space between attribute name and "="',
                event.line,
                col + attr.index + indexOfEqualSign - 1,
                self,
                attr.raw
              );
            }

            if (rawAttr.charAt(indexOfEqualSign + 1) === ' ') {
              reporter.warn(
                'Space between "=" and attribute value',
                event.line,
                col + attr.index + indexOfEqualSign + 1,
                self,
                attr.raw
              );
            }
          });
      }

      parser.addListener('tagstart', handleTagStart);
    }
  });
};
