/**
 * Links with target="_blank" will open a new window with partial access to the
 * original window, enabling clickjacking attacks. In order to prevent it,
 * every anchor tag with target="_blank" should also have rel="noopener
 * noreferrer".
 * See this blog post for more details:
 * https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/
 */
module.exports = function(HTMLHint) {
  HTMLHint.addRule({
    id: 'no-clickjacking-on-target-blank',
    description:
      'Anchor tags with target="_blank" must also have rel="noopener noreferrer" to prevent clickjacking attacks',
    init: function(parser, reporter) {
      var self = this;

      function isTargetBlankAttr(attr) {
        return attr.name === 'target' && attr.value === '_blank';
      }

      function isRelNoOpenerAttr(attr) {
        return attr.name === 'rel' && attr.value === 'noopener noreferrer';
      }

      function handleTagStart(event) {
        if (event.tagName === 'a') {
          event.attrs.filter(isTargetBlankAttr).forEach(function(attr) {
            var hasRelNoOpener =
              event.attrs.filter(isRelNoOpenerAttr).length > 0;
            if (!hasRelNoOpener) {
              var col = event.col + event.tagName.length + 1;
              reporter.warn(
                'Anchor tags with [target="_blank"] must also have [rel="noopener noreferrer"]',
                event.line,
                col + attr.index,
                self,
                attr.raw
              );
            }
          });
        }
      }

      parser.addListener('tagstart', handleTagStart);
    }
  });
};
