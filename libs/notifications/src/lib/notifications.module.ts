import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { TRANSLOCO_SCOPE, TranslocoModule } from '@ngneat/transloco';
import { loader } from './transloco.loader';

import { NotificationEffects } from './+state/notification.effects';
import { NotificationFacade } from './+state/notification.facade';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    StoreModule,
    EffectsModule.forFeature([NotificationEffects]),
    TranslocoModule
  ],
  providers: [
    NotificationFacade,
    {
      provide: TRANSLOCO_SCOPE,
      useValue: { scope: 'notification-scope', loader }
    }
  ]
})
export class NotificationsModule {}
