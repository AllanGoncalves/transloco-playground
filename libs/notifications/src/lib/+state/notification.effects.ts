import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { TranslocoService } from '@ngneat/transloco';

import * as notificationActions from './notification.actions';

@Injectable()
export class NotificationEffects {
  openNotification$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(notificationActions.openNotification),
        tap(({ message }) =>
          this.snackBar.open(message, 'dismiss', { duration: 2000 })
        )
      ),
    { dispatch: false }
  );

  openSuccessNotification$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(notificationActions.openSuccessNotification),
        tap(() => {
          const message = this.translocoService.translate(
            'successNotification',
            {},
            'notification-scope'
          );
          this.snackBar.open(message, 'dismiss', { duration: 2000 });
        })
      ),
    { dispatch: false }
  );

  openFailureNotification$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(notificationActions.openFailureNotification),
        tap(() => {
          const message = this.translocoService.translate(
            'failureNotification',
            {},
            'notification-scope'
          );
          this.snackBar.open(message, 'dismiss', { duration: 2000 });
        })
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private snackBar: MatSnackBar, private translocoService: TranslocoService) {}
}
