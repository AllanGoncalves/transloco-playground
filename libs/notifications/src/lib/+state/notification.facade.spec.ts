import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';

import * as notificationActions from './notification.actions';
import { NotificationFacade } from './notification.facade';

describe('NotificationFacade', () => {
  let facade: NotificationFacade;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({}), NotificationFacade]
    });

    facade = TestBed.get(NotificationFacade);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch');
  });

  describe('openNotification', () => {
    it('should call store.dispatch with openNotification action', () => {
      const message = 'My message';
      const expected = notificationActions.openNotification({ message });
      facade.openNotification(message);
      expect(store.dispatch).toHaveBeenCalledWith(expected);
    });
  });

  describe('openSuccessNotification', () => {
    it('should call store.dispatch with openSuccessNotification action', () => {
      const expected = notificationActions.openSuccessNotification();
      facade.openSuccessNotification();
      expect(store.dispatch).toHaveBeenCalledWith(expected);
    });
  });

  describe('openFailureNotification', () => {
    it('should call store.dispatch with openFailureNotification action', () => {
      const expected = notificationActions.openFailureNotification();
      facade.openFailureNotification();
      expect(store.dispatch).toHaveBeenCalledWith(expected);
    });
  });
});
