import { Action, createAction, props } from '@ngrx/store';
import { Notification } from './notification.models';

const PREFIX = '[Notifications]';

export const openNotification = createAction(
  `${PREFIX} Open Notification`,
  props<Notification>()
);

export const openSuccessNotification = createAction(
  `${PREFIX} Open Success Notification`
);

export const openFailureNotification = createAction(
  `${PREFIX} Open Failure Notification`
);
