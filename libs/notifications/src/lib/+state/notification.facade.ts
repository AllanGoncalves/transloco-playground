import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import * as notificationActions from './notification.actions';

@Injectable()
export class NotificationFacade {
  constructor(private store: Store<any>) {}

  openNotification(message: string): void {
    this.store.dispatch(notificationActions.openNotification({ message }));
  }

  openSuccessNotification(): void {
    this.store.dispatch(notificationActions.openSuccessNotification());
  }

  openFailureNotification(): void {
    this.store.dispatch(notificationActions.openFailureNotification());
  }
}
