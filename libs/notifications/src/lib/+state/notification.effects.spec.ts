import { TestBed, async } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

import { provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { hot, cold } from 'jasmine-marbles';

import { getTranslocoModule } from '@transloco-playground/utils';
import en from '../i18n/notification-scope/en.json';

import { NotificationEffects } from './notification.effects';
import * as notificationActions from './notification.actions';

describe('NotificationEffects', () => {
  let actions$: Observable<any>;
  let effects: NotificationEffects;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        getTranslocoModule(en, 'notificationScope', ['en'])
      ],
      providers: [
        NotificationEffects,
        provideMockStore({}),
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(NotificationEffects);
    snackBar = TestBed.get(MatSnackBar);
    spyOn(snackBar, 'open');
  });

  describe('openNotification$', () => {
    it('should open snack bar with given message', () => {
      const message = 'My message';
      const action = notificationActions.openNotification({ message });
      actions$ = hot('-a', { a: action });
      const expected$ = cold('-c', { c: action });
      expect(effects.openNotification$).toBeObservable(expected$);
      expect(snackBar.open).toHaveBeenCalledWith(
        message,
        jasmine.any(String),
        jasmine.any(Object)
      );
    });
  });

  describe('openSuccessNotification$', () => {
    it('should open snack bar with success message', () => {
      const action = notificationActions.openSuccessNotification();
      actions$ = hot('-a', { a: action });
      const expected$ = cold('-c', { c: action });
      expect(effects.openSuccessNotification$).toBeObservable(expected$);
      expect(snackBar.open).toHaveBeenCalledWith(
        jasmine.stringMatching(en.successNotification),
        jasmine.any(String),
        jasmine.any(Object)
      );
    });
  });

  describe('openFailureNotification$', () => {
    it('should open snack bar with failure message', () => {
      const message = 'My message';
      const action = notificationActions.openFailureNotification();
      actions$ = hot('-a', { a: action });
      const expected$ = cold('-c', { c: action });
      expect(effects.openFailureNotification$).toBeObservable(expected$);
      expect(snackBar.open).toHaveBeenCalledWith(
        jasmine.stringMatching(en.failureNotification),
        jasmine.any(String),
        jasmine.any(Object)
      );
    });
  });
});
