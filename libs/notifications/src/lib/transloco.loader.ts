export const loader = ['en'].reduce((acc: any, lang: string) => {
  acc[lang] = () => import(`./i18n/notification-scope/${lang}.json`);
  return acc;
}, {});
