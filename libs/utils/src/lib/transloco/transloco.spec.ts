import { formatMockTranslation, createMockTranslations } from './transloco';

describe('Transloco utils', () => {
  describe('formatMockTranslation', () => {
    it('should add scope to translation keys, and suffix to values', () => {
      const scope = 'scope';
      const suffix = ' - suffix';
      const translation = {
        foo: 'Foo',
        bar: 'Bar'
      };
      const expected = {
        [`${scope}.foo`]: `${translation.foo}${suffix}`,
        [`${scope}.bar`]: `${translation.bar}${suffix}`
      };
      expect(formatMockTranslation(translation, scope, suffix)).toEqual(
        expected
      );
    });
  });

  describe('createMockTranslations', () => {
    it('should generate translations for the given languages', () => {
      const translation = {
        foo: 'Foo',
        bar: 'Bar'
      };
      const scope = 'scope';
      const languages = ['en', 'es', 'pt'];
      const expected = {
        en: {
          [`${scope}.foo`]: `${translation.foo} - en`,
          [`${scope}.bar`]: `${translation.bar} - en`
        },
        es: {
          [`${scope}.foo`]: `${translation.foo} - es`,
          [`${scope}.bar`]: `${translation.bar} - es`
        },
        pt: {
          [`${scope}.foo`]: `${translation.foo} - pt`,
          [`${scope}.bar`]: `${translation.bar} - pt`
        }
      };

      expect(createMockTranslations(translation, scope, languages)).toEqual(
        expected
      );
    });
  });
});
