import { TranslocoConfig, TranslocoTestingModule } from '@ngneat/transloco';

interface Dictionary<T> {
  [key: string]: T;
}

export function formatMockTranslation(
  translation: Dictionary<string>,
  scope: string = '',
  valueSuffix: string = ''
): Dictionary<string> {
  return Object.entries(translation).reduce((acc, [key, value]) => {
    acc[`${scope}.${key}`] = `${value}${valueSuffix}`;
    return acc;
  }, {});
}

export function createMockTranslations(
  translation: Dictionary<string>,
  scope: string = '',
  languages: string[] = ['en', 'es']
): Dictionary<Dictionary<string>> {
  return languages.reduce((acc, language) => {
    acc[language] = formatMockTranslation(translation, scope, ` - ${language}`);
    return acc;
  }, {});
}

export function getTranslocoModule(
  translation: Dictionary<string>,
  scope: string = '',
  languages: string[] = ['en', 'es'],
  config: Partial<TranslocoConfig> = {}
) {
  return TranslocoTestingModule.withLangs(
    createMockTranslations(translation, scope, languages),
    {
      availableLangs: languages,
      defaultLang: languages[0],
      ...config
    }
  );
}
