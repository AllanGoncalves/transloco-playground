import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';

import { NotificationFacade } from '@transloco-playground/notifications';
import { getTranslocoModule } from '@transloco-playground/utils';
import en from '../i18n/lazy-scope/en.json';
import { LazyComponent } from './lazy.component';

class MockNotificationFacade {
  openSuccessNotification() {}
  openFailureNotification() {}
}

describe('LazyComponent', () => {
  let component: LazyComponent;
  let fixture: ComponentFixture<LazyComponent>;
  let notificationFacade: MockNotificationFacade;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatButtonModule, getTranslocoModule(en, 'lazyScope', ['en'])],
      declarations: [LazyComponent],
      providers: [
        { provide: NotificationFacade, useClass: MockNotificationFacade }
      ]
    }).compileComponents();

    notificationFacade = TestBed.get(NotificationFacade);
    spyOn(notificationFacade, 'openSuccessNotification');
    spyOn(notificationFacade, 'openFailureNotification');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('translations', () => {
    it('should display', () => {
      const title = fixture.debugElement.query(By.css('h2'));
      expect(title.nativeElement.textContent).toContain(en.lazyTitle);

      const successNotificationButton = fixture.debugElement.query(
        By.css('button[color="primary"]')
      );
      expect(successNotificationButton.nativeElement.textContent).toContain(
        en.openSuccessNotificationButton
      );

      const failureNotificationButton = fixture.debugElement.query(
        By.css('button[color="warn"]')
      );
      expect(failureNotificationButton.nativeElement.textContent).toContain(
        en.openFailureNotificationButton
      );
    });
  });

  describe('openSuccessNotification', () => {
    it('should call notificationFacade.openSuccessNotification', () => {
      component.openSuccessNotification();
      expect(notificationFacade.openSuccessNotification).toHaveBeenCalled();
    });
  });

  describe('openFailureNotification', () => {
    it('should call notificationFacade.openFailureNotification', () => {
      component.openFailureNotification();
      expect(notificationFacade.openFailureNotification).toHaveBeenCalled();
    });
  });
});
