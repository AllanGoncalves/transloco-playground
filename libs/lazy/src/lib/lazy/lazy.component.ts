import { Component, OnInit } from '@angular/core';
import { NotificationFacade } from '@transloco-playground/notifications';

@Component({
  selector: 'transloco-playground-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: ['./lazy.component.scss']
})
export class LazyComponent implements OnInit {
  constructor(private notificationFacade: NotificationFacade) {}

  ngOnInit() {}

  openSuccessNotification(): void {
    this.notificationFacade.openSuccessNotification();
  }

  openFailureNotification(): void {
    this.notificationFacade.openFailureNotification();
  }
}
