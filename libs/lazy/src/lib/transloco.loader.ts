export const loader = ['en'].reduce((acc: any, lang: string) => {
  acc[lang] = () => import(`./i18n/lazy-scope/${lang}.json`);
  return acc;
}, {});
