import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { TRANSLOCO_SCOPE, TranslocoModule } from '@ngneat/transloco';

import { NotificationsModule } from '@transloco-playground/notifications';
import { LazyComponent } from './lazy/lazy.component';
import { loader } from './transloco.loader';

const routes: Routes = [
  {
    path: '',
    component: LazyComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    TranslocoModule,
    NotificationsModule
  ],
  declarations: [LazyComponent],
  providers: [
    { provide: TRANSLOCO_SCOPE, useValue: { scope: 'lazy-scope', loader } }
  ]
})
export class LazyModule {}
